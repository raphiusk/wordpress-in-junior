<section>
<hr>    
<div class="card-post" id="card-post">          
<h1> Alguns de nossos lanches </h1>
            <?php
                $args = array(
                    'post_type'         => 'novidades',
                    'post_status'       => 'publish',
                    'suppress_filters'  => true,
                    'orderby'           => 'post_date',
                    'order'             => 'DESC'
                );
                
                $news = new WP_Query( $args );                
                if ( $news -> have_posts() ) : 
                    while ( $news -> have_posts() ) : $news -> the_post(); ?>
                            <h2>
                                <a href="<?php the_permalink() ?>  "> 
                                    <?php the_title();?> 
                                </a>
                            </h2>
                            <h3> Publicado: <?php the_time('j / m / Y'); ?> às <?php the_time('G:i')?> por <?php the_author_posts_link(); ?></h3>
                        <div class="card-post-topic" id="card-post-topic">
                            <h4><?php the_content(); ?></h4>  
                        </div>
                <?php 
                    endwhile; 
                    else : 
                    esc_html_e( 'Nenhum post encontrado.' );           
                endif; ?>
                <br>
                <?php
                $big = 9999999999;
                echo paginate_links(
                    array(
                        'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format'    => '?paged=%#%',
                        'current'   => max( 1, get_query_var( 'paged' ) ),
                        'total'     => $news -> max_num_pages
                    )
                );
            ?>
        </div>
</section>