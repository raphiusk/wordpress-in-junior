<?php get_header() ?>
    <main>
    <div class="card-post" id="card-post">
        <?php get_search_form();?> 
            <?php 
                
                if ( have_posts() ) : 
                    while ( have_posts() ) : the_post(); ?>
                            <br>
                            <hr>
                            <br>
                            <p><a href="<?php the_permalink() ?>  "> <?php the_title();?> </a></p>
                            <p>Publicado: <?php the_time('j / m / Y'); ?> às <?php the_time('G:i')?> por <?php the_author_posts_link(); ?></p>
                            <div class="card-post-topic" id="card-post-topic">
                            <?php #the_content(); ?>
                           <hr>
                           </div>
            <?php 
                    endwhile; 
                else : ?>
                <p><?php esc_html_e( 'Nenhum post encontrado.' ); ?></p>
            <?php 
                endif; 
            ?>       
        <div><p><?php echo paginate_links();?></p></div>
        </div>
    </main>
<?php get_footer() ?>

