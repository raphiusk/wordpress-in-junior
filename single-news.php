<?php
/*
    Template Name: Custom Posts
*/ 
?> 

<?php get_header() ?>
    <main>
    <div class="card-post" id="card-post">
        <?php get_search_form();?> 
            <section>
                <h1><?php the_field('titulo'); ?></h1>    
                <?php $image = (get_field('imagem')) ?>
                    <img src="<?php echo $image['url']?>" width="echo $image['width']" height="echo $image['height']" alt="">    
                <p><?php the_field('mensagem'); ?></p>
            </section> 
            <p><?php get_template_part('inc', 'section')?></p>
    </div>
<div><p><?php echo paginate_links();?></p></div>
    </main>
<?php get_footer() ?>