<?php
    /*
    Template Name: Posts
    */ 
?>

<?php get_header() ?>
    <main>   
            <div class="card-post" id="card-post">
            <?php get_search_form()?>
            <?php if ( have_posts() ) : 
                    while ( have_posts() ) : the_post(); ?>
                            <h2><a href="<?php the_permalink() ?>  "> <?php the_title();?> </a></h2>
                            <h3>Publicado: <?php the_time('j / m / Y'); ?> às <?php the_time('G:i')?> por <?php the_author_posts_link(); ?></h3>
                            <div class="card-post-topic" id="card-post-topic">
                            <h4><?php the_content(); ?></h4>
            </div>
            <?php 
                    endwhile; 
                else : ?>
                <p><?php esc_html_e( 'Nenhum post encontrado.' ); ?></p>
            <?php 
                endif; 
            ?>
            <br>
            <?php echo paginate_links();?>
        </div>         
    </main>
<?php get_footer() ?>

