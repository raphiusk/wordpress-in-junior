<?php 
    add_theme_support( 'title-tag' );
    add_theme_support( 'menus' );

    function guri_faminto_setup(){
        
    }

    function guri_faminto_scripts(){
        #localização do reset.css e style.css
        wp_enqueue_style( 'guri-faminto-reset', get_template_directory_uri().'/reset.css' ); 
        wp_enqueue_style( 'guri-faminto-style', get_template_directory_uri().'/style.css' );
        #wp_enqueue_style( 'guri-faminto-style', get_template_directory_uri().'/script.js' );
    }

    #executa a função com os scripts
    add_action('wp_enqueue_scripts', 'guri_faminto_scripts');
    

    add_theme_support('title-tag');
    add_theme_support('menus');
    add_theme_support('post-thumbnails', array('novidades'));

    function custom_post_type_novidades() {
        register_post_type('novidades', array(
            'label' => 'Novidades',
            'description' => 'Descrição novidades',
            'menu_position' =>  2,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'query_var' => true,
            'supports' => array('title', 'editor', 'thumbnail'),
     
            'labels' => array (
                'name' => 'Novidade',
                'singular_name' => 'Novidade',
                'menu_name' => 'Novidades',
                'add_new' => 'Criar uma novidade',
                'add_new_item' => 'Adicionar uma Novidade',
                'edit' => 'Editar Novidade',
                'edit_item' => 'Editar Novidade',
                'new_item' => 'Nova Novidade',
                'view' => 'Ver Novidade',
                'view_item' => 'Ver Novidade',
                'search_items' => 'Procurar Novidade',
                'not_found' => 'Nenhuma Novidade Encontrada',
                'not_found_in_trash' => 'Nenhuma Novidade Encontrada no Lixo',
            )
        )); 
    }
    add_action( 'init', 'custom_post_type_novidades');

?>